﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace DungeonDex.Utils
{
    public class DatabaseHandler
    {
        /// <summary>
        /// Handles all database operations
        /// </summary>
        /// 
        SQLiteConnection m_sqlConnection;
        string m_dbPath = @"db.sqlite3";

        public DatabaseHandler()
        {

            if (!File.Exists(m_dbPath))
            {
                InitDatabase();
            }
            else
            {
                m_sqlConnection = new SQLiteConnection("Data Source=db.sqlite3;version=3");
                m_sqlConnection.Open();
            }

        }

        void InitDatabase()
        {

            SQLiteConnection.CreateFile(m_dbPath);
            m_sqlConnection = new SQLiteConnection("Data Source=db.sqlite3;version=3");
            m_sqlConnection.Open();
            string sql = "CREATE TABLE CHARACTERS (" +
                "id text primary key," +
                "name text," +
                "class text," +
                "level int," +
                "background text," +
                "race text," +
                "alignment text," +
                "exp int," +
                "profBonus int," +
                "inspiration bit," +
                "str int," +
                "dex int," +
                "const int," +
                "intel int," +
                "wisdom int," +
                "charisma int," +
                "ath int," +
                "acro int," +
                "soh int," +
                "stealth int," +
                "arcana int," +
                "history int," +
                "invest int," +
                "nature int," +
                "relig int," +
                "ah int," +
                "insight int," +
                "med int," +
                "percep int," +
                "survival int," +
                "decept int," +
                "intimidation int," +
                "perf int," +
                "persuasion int," +
                "ac int," +
                "shield int," +
                "ac_misc int," +
                "initiative int," +
                "spd int," +
                "hp_max int," +
                "hp_current int," +
                "hitdie_type int," +
                "hitdie_count int," +
                "ds_successes int," +
                "ds_fails int," +
                "w1_name text," +
                "w1_atkbns int," +
                "w1_damagetype text," +
                "w2_name text," +
                "w2_atkbns int," +
                "w2_damagetype text," +
                "w3_name text," +
                "w3_atkbns int," +
                "w3_damagetype text," +
                "items text," +
                "feats text," +
                "features string," +
                "a_items string)";
            SQLiteCommand command = new SQLiteCommand(sql, m_sqlConnection);
            command.ExecuteNonQuery();

        }

        public CharacterModel LoadCharacter(string id)
        {
            string sql = "select * from CHARACTERS where id = \"" + id + "\"";
            SQLiteCommand command = new SQLiteCommand(sql, m_sqlConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            //insert info into new CharacterModel
            reader.Read();
            CharacterModel c = new CharacterModel(true)
            {
                //// first strings
                ID1 = reader["id"].ToString(),
                Name = reader["name"].ToString(),
                C_class = reader["class"].ToString(),
                Race = reader["race"].ToString(),
                Background = reader["background"].ToString(),
                Alignment = reader["alignment"].ToString(),
                W1_name = reader["w1_name"].ToString(),
                W1_damagetype = reader["w1_damagetype"].ToString(),
                W2_name = reader["w2_name"].ToString(),
                W2_damagetype = reader["w2_damagetype"].ToString(),
                W3_name = reader["w3_name"].ToString(),
                W3_damagetype = reader["w3_damagetype"].ToString(),
                Items = reader["items"].ToString(),
                Feats = reader["feats"].ToString(),
                Features = reader["features"].ToString(),
                A_items = reader["a_items"].ToString(),
                /////// now integers
                Level = Convert.ToInt32(reader["level"]),
                Exp = Convert.ToInt32(reader["exp"]),
                ProfBonus = Convert.ToInt32(reader["profBonus"]),
                Inspiration = Convert.ToInt32(reader["inspiration"]),
                Str = Convert.ToInt32(reader["str"]),
                Dex = Convert.ToInt32(reader["dex"]),
                Constitution = Convert.ToInt32(reader["const"]),
                Intel = Convert.ToInt32(reader["intel"]),
                Wisdom = Convert.ToInt32(reader["wisdom"]),
                Charisma = Convert.ToInt32(reader["charisma"]),
                Ath = Convert.ToInt32(reader["ath"]),
                Acro = Convert.ToInt32(reader["acro"]),
                Soh = Convert.ToInt32(reader["soh"]),
                Stealth = Convert.ToInt32(reader["stealth"]),
                Arcana = Convert.ToInt32(reader["arcana"]),
                History = Convert.ToInt32(reader["history"]),
                Invest = Convert.ToInt32(reader["invest"]),
                Nature = Convert.ToInt32(reader["nature"]),
                Religion = Convert.ToInt32(reader["relig"]),
                AnimalHandling = Convert.ToInt32(reader["ah"]),
                Insight = Convert.ToInt32(reader["insight"]),
                Med = Convert.ToInt32(reader["med"]),
                Percept = Convert.ToInt32(reader["percep"]),
                Survival = Convert.ToInt32(reader["survival"]),
                Decept = Convert.ToInt32(reader["decept"]),
                Intimidation = Convert.ToInt32(reader["intimidation"]),
                Perf = Convert.ToInt32(reader["perf"]),
                Persuasion = Convert.ToInt32(reader["persuasion"]),
                Ac = Convert.ToInt32(reader["ac"]),
                Shield = Convert.ToInt32(reader["shield"]),
                Ac_misc = Convert.ToInt32(reader["ac_misc"]),
                Initiative = Convert.ToInt32(reader["initiative"]),
                Spd = Convert.ToInt32(reader["spd"]),
                Hp_max = Convert.ToInt32(reader["hp_max"]),
                Hp_current = Convert.ToInt32(reader["hp_current"]),
                Hitdie_type = Convert.ToInt32(reader["hitdie_type"]),
                Hitdie_count = Convert.ToInt32(reader["hitdie_count"]),
                Ds_successes = Convert.ToInt32(reader["ds_successes"]),
                Ds_fails = Convert.ToInt32(reader["ds_fails"]),
                W1_atkbns = Convert.ToInt32(reader["w1_atkbns"]),
                W2_atkbns = Convert.ToInt32(reader["w2_atkbns"]),
                W3_atkbns = Convert.ToInt32(reader["w3_atkbns"])

            };


            return c;
        }

        public void SaveCharacter(CharacterModel c)
        {

            Dictionary<string, string> ids = ListCharacters();
            string sql;

            if (ids.Any(i => i.Key == c.ID1))
            {
                sql = "update CHARACTERS set name = \"" + c.Name.ToString() + "\", " +
                "class = \"" + c.Name.ToString() + "\", " +
                "level = " + c.Level.ToString() + ", " +
                "background = \"" + c.Background.ToString() + "\", " +
                "race = \"" + c.Race.ToString() + "\", " +
                "alignment = \"" + c.Alignment.ToString() + "\", " +
                "exp = " + c.Exp.ToString() + ", " +
                "profBonus = " + c.ProfBonus.ToString() + ", " +
                "inspiration  = " + c.Inspiration.ToString() + ", " +
                "str = " + c.Str.ToString() + ", " +
                "dex = " + c.Dex.ToString() + ", " +
                "const = " + c.Constitution.ToString() + ", " +
                "intel = " + c.Intel.ToString() + ", " +
                "wisdom = " + c.Wisdom.ToString() + ", " +
                "charisma = " + c.Charisma.ToString() + ", " +
                "ath = " + c.Ath.ToString() + ", " +
                "acro = " + c.Acro.ToString() + ", " +
                "soh = " + c.Soh.ToString() + ", " +
                "stealth = " + c.Stealth.ToString() + ", " +
                "arcana = " + c.Arcana.ToString() + ", " +
                "history = " + c.History.ToString() + ", " +
                "invest = " + c.Invest.ToString() + ", " +
                "nature = " + c.Nature.ToString() + ", " +
                "relig = " + c.Religion.ToString() + ", " +
                "ah = " + c.AnimalHandling.ToString() + ", " +
                "insight = " + c.Insight.ToString() + ", " +
                "med = " + c.Med.ToString() + ", " +
                "percep = " + c.Percept.ToString() + ", " +
                "survival = " + c.Survival.ToString() + ", " +
                "decept = " + c.Decept.ToString() + ", " +
                "intimidation = " + c.Intimidation.ToString() + ", " +
                "perf = " + c.Perf.ToString() + ", " +
                "persuasion = " + c.Persuasion.ToString() + ", " +
                "ac = " + c.Ac.ToString() + ", " +
                "shield = " + c.Shield.ToString() + ", " +
                "ac_misc = " + c.Ac_misc.ToString() + ", " +
                "initiative = " + c.Initiative.ToString() + ", " +
                "spd = " + c.Spd.ToString() + ", " +
                "hp_max = " + c.Hp_max.ToString() + ", " +
                "hp_current = " + c.Hp_current.ToString() + ", " +
                "hitdie_type = " + c.Hitdie_type.ToString() + ", " +
                "hitdie_count = " + c.Hitdie_count.ToString() + ", " +
                "ds_successes = " + c.Ds_successes.ToString() + ", " +
                "ds_fails = " + c.Ds_fails.ToString() + ", " +
                "w1_name = \"" + c.W1_name.ToString() + "\", " +
                "w1_atkbns = " + c.W1_atkbns.ToString() + ", " +
                "w1_damagetype = \"" + c.W1_damagetype.ToString() + "\", " +
                "w2_name = \"" + c.W2_name.ToString() + "\", " +
                "w2_atkbns = " + c.W2_atkbns.ToString() + ", " +
                "w2_damagetype = \"" + c.W2_damagetype.ToString() + "\", " +
                "w3_name = \"" + c.W3_name.ToString() + "\", " +
                "w3_atkbns = " + c.W3_atkbns.ToString() + ", " +
                "w3_damagetype = \"" + c.W3_damagetype.ToString() + "\", " +
                "items = \"" + c.Items.ToString() + "\", " +
                "feats = \"" + c.Feats.ToString() + "\"," +
                "features = \"" + c.Features.ToString() + "\"," +
                "a_items = \"" + c.A_items.ToString() +"\"" +
                "where id = \"" + c.ID1 + "\"";
            }
            else
            {
                sql = "INSERT INTO CHARACTERS (ID, name, class, race, background, alignment, w1_name, w1_damagetype, w2_name, w2_damagetype, w3_name, w3_damagetype, items, features, a_items, feats, " +
                    "level, exp, profBonus, inspiration, str, dex, const, intel, wisdom, charisma, ath, acro, soh, stealth, arcana, history," +
                    "invest, nature, relig, ah, insight, med, percep, survival, decept, intimidation, perf, persuasion, ac, shield, ac_misc, initiative," +
                    "spd, hp_max, hp_current, hitdie_type, hitdie_count, ds_successes, ds_fails, w1_atkbns, w2_atkbns, w3_atkbns) VALUES (" +
                     $"\"{c.ID1}\", \"" +
                     c.Name + "\", \"" +
                     c.C_class + "\", \"" +
                     c.Race + "\", \"" +
                     c.Background + "\", \"" +
                     c.Alignment + "\", \"" +
                     c.W1_name + "\", \"" +
                     c.W1_damagetype + "\", \"" +
                     c.W2_name + "\", \"" +
                     c.W2_damagetype + "\", \"" +
                     c.W3_name + "\", \"" +
                     c.W3_damagetype + "\", \"" +
                     c.Items + "\", \"" +
                     c.Features + "\", \"" +
                     c.A_items + "\", \"" +
                     c.Feats + "\", " +
                     c.Level + ", " +
                     c.Exp + ", " +
                     c.ProfBonus + ", " +
                     c.Inspiration + ", " +
                     c.Str + ", " +
                     c.Dex + ", " +
                     c.Constitution + ", " +
                     c.Intel + ", " +
                     c.Wisdom + ", " +
                     c.Charisma + ", " +
                     c.Ath + ", " +
                     c.Acro + ", " +
                     c.Soh + ", " +
                     c.Stealth + ", " +
                     c.Arcana + ", " +
                     c.History + ", " +
                     c.Invest + ", " +
                     c.Nature + ", " +
                     c.Religion + ", " +
                     c.AnimalHandling + ", " +
                     c.Insight + ", " +
                     c.Med + ", " +
                     c.Percept + ", " +
                     c.Survival + ", " +
                     c.Decept + ", " +
                     c.Intimidation + ", " +
                     c.Perf + ", " +
                     c.Persuasion + ", " +
                     c.Ac + ", " +
                     c.Shield + ", " +
                     c.Ac_misc + ", " +
                     c.Initiative + ", " +
                     c.Spd + ", " +
                     c.Hp_max + ", " +
                     c.Hp_current + ", " +
                     c.Hitdie_type + ", " +
                     c.Hitdie_count + ", " +
                     c.Ds_successes + ", " +
                     c.Ds_fails + ", " +
                     c.W1_atkbns + ", " +
                     c.W2_atkbns + ", " +
                     c.W3_atkbns +
                    ")";
            }

            SQLiteCommand command = new SQLiteCommand(sql, m_sqlConnection);
            command.ExecuteNonQuery();
        }

        public Dictionary<string, string> ListCharacters()
        {
            string sql = "select id, name from CHARACTERS";
            Dictionary<string, string> idsWithName = new Dictionary<string, string>();

            SQLiteCommand command = new SQLiteCommand(sql, m_sqlConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                idsWithName.Add(reader["id"].ToString(), reader["name"].ToString());
            }
            return idsWithName;
        }
    }
}
