﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDex.Utils
{
    public class CharacterModel
    {
        string ID = "";
        string name = "";
        string c_class = "";
        string race = "";
        string background = "";
        string alignment = "";
        string w1_name = "";
        string w1_damagetype = "";
        string w2_name = "";
        string w2_damagetype = "";
        string w3_name = "";
        string w3_damagetype = "";
        string items = "";
        string feats = "";
        string features = "";
        string a_items = "";

        int level, exp, profBonus, inspiration, str, dex, constitution, intel, wisdom, charisma, ath, acro, soh, stealth, arcana, history, 
            invest, nature, religon, ah, insight, med, percept, survival, decept, intimidation, perf, persuasion, ac, shield, ac_misc, initiative, 
            spd, hp_max, hp_current, hitdie_type, hitdie_count, ds_successes, ds_fails, w1_atkbns, w2_atkbns, w3_atkbns; 

        public int Level { get => level; set => level = value; }
        public int Exp { get => exp; set => exp = value; }
        public int ProfBonus { get => profBonus; set => profBonus = value; }
        public int Inspiration { get => inspiration; set => inspiration = value; }
        public int Str { get => str; set => str = value; }
        public int Dex { get => dex; set => dex = value; }
        public int Constitution { get => constitution; set => constitution = value; }
        public int Intel { get => intel; set => intel = value; }
        public int Wisdom { get => wisdom; set => wisdom = value; }
        public int Charisma { get => charisma; set => charisma = value; }
        public int Ath { get => ath; set => ath = value; }
        public int Acro { get => acro; set => acro = value; }
        public int Soh { get => soh; set => soh = value; }
        public int Stealth { get => stealth; set => stealth = value; }
        public int Arcana { get => arcana; set => arcana = value; }
        public int History { get => history; set => history = value; }
        public int Invest { get => invest; set => invest = value; }
        public int Nature { get => nature; set => nature = value; }
        public int Religion { get => religon; set => religon = value; }
        public int AnimalHandling { get => ah; set => ah = value; }
        public int Insight { get => insight; set => insight = value; }
        public int Med { get => med; set => med = value; }
        public int Percept { get => percept; set => percept = value; }
        public int Survival { get => survival; set => survival = value; }
        public int Decept { get => decept; set => decept = value; }
        public int Intimidation { get => intimidation; set => intimidation = value; }
        public int Perf { get => perf; set => perf = value; }
        public int Persuasion { get => persuasion; set => persuasion = value; }
        public int Ac { get => ac; set => ac = value; }
        public int Shield { get => shield; set => shield = value; }
        public int Ac_misc { get => ac_misc; set => ac_misc = value; }
        public int Initiative { get => initiative; set => initiative = value; }
        public int Spd { get => spd; set => spd = value; }
        public int Hp_max { get => hp_max; set => hp_max = value; }
        public int Hp_current { get => hp_current; set => hp_current = value; }
        public int Hitdie_type { get => hitdie_type; set => hitdie_type = value; }
        public int Hitdie_count { get => hitdie_count; set => hitdie_count = value; }
        public int Ds_successes { get => ds_successes; set => ds_successes = value; }
        public int Ds_fails { get => ds_fails; set => ds_fails = value; }
        public int W1_atkbns { get => w1_atkbns; set => w1_atkbns = value; }
        public int W2_atkbns { get => w2_atkbns; set => w2_atkbns = value; }
        public int W3_atkbns { get => w3_atkbns; set => w3_atkbns = value; }
        public string ID1 { get => ID; set => ID = value; }
        public string Name { get => name; set => name = value; }
        public string C_class { get => c_class; set => c_class = value; }
        public string Race { get => race; set => race = value; }
        public string Background { get => background; set => background = value; }
        public string Alignment { get => alignment; set => alignment = value; }
        public string W1_name { get => w1_name; set => w1_name = value; }
        public string W1_damagetype { get => w1_damagetype; set => w1_damagetype = value; }
        public string W2_name { get => w2_name; set => w2_name = value; }
        public string W2_damagetype { get => w2_damagetype; set => w2_damagetype = value; }
        public string W3_name { get => w3_name; set => w3_name = value; }
        public string W3_damagetype { get => w3_damagetype; set => w3_damagetype = value; }
        public string Items { get => items; set => items = value; }
        public string Feats { get => feats; set => feats = value; }
        public string Features { get => features; set => features = value; }
        public string A_items { get => a_items; set => a_items = value; }

        public CharacterModel()
        {
            ID1 = Guid.NewGuid().ToString().Replace("-", "i");
        }

        public CharacterModel(bool b)
        {

        }

        public CharacterModel(string id)
        {
            ID1 = id;
        }
    }
}
