﻿namespace DungeonDex
{
    partial class CharacterSheet
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CharacterSheet));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton20 = new System.Windows.Forms.RadioButton();
            this.radioButton21 = new System.Windows.Forms.RadioButton();
            this.radioButton22 = new System.Windows.Forms.RadioButton();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.featurePanel = new System.Windows.Forms.Panel();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label85 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.radioButton27 = new System.Windows.Forms.RadioButton();
            this.radioButton25 = new System.Windows.Forms.RadioButton();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.radioButton26 = new System.Windows.Forms.RadioButton();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.panel13 = new System.Windows.Forms.Panel();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.currPanel = new System.Windows.Forms.Panel();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.accItemsPanel = new System.Windows.Forms.Panel();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.featsPanel = new System.Windows.Forms.Panel();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label78 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.featurePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.currPanel.SuspendLayout();
            this.accItemsPanel.SuspendLayout();
            this.featsPanel.SuspendLayout();
            this.panel16.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(70, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "PROFICIENCY BONUS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "INSPIRATION";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(19, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(40, 29);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(19, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(40, 29);
            this.textBox2.TabIndex = 2;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(19, 31);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(40, 29);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(19, 31);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(40, 29);
            this.textBox4.TabIndex = 2;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(19, 31);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(40, 29);
            this.textBox5.TabIndex = 2;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(19, 31);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(40, 29);
            this.textBox7.TabIndex = 2;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox7.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(19, 31);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(40, 29);
            this.textBox8.TabIndex = 2;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "STRENGTH";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "DEXTERITY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "CONSTITUTION";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "WISDOM";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "CHARISMA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(106, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 12);
            this.label9.TabIndex = 1;
            this.label9.Text = "SAVING THROWS";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(106, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 12);
            this.label10.TabIndex = 1;
            this.label10.Text = "ATHLETICS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(106, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 12);
            this.label11.TabIndex = 1;
            this.label11.Text = "SAVING THROWS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(106, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 12);
            this.label12.TabIndex = 1;
            this.label12.Text = "ACROBATICS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(106, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 1;
            this.label13.Text = "SLEIGHT OF HAND";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(106, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "STEALTH";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(106, 1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 12);
            this.label15.TabIndex = 1;
            this.label15.Text = "SAVING THROWS";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(106, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 12);
            this.label16.TabIndex = 1;
            this.label16.Text = "SAVING THROWS";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(106, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 12);
            this.label17.TabIndex = 1;
            this.label17.Text = "ARCANA";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(106, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 12);
            this.label18.TabIndex = 1;
            this.label18.Text = "HISTORY";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(106, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 12);
            this.label22.TabIndex = 1;
            this.label22.Text = "SAVING THROWS";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(106, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 12);
            this.label23.TabIndex = 1;
            this.label23.Text = "ANIMAL HANDLING";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(106, 37);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 12);
            this.label24.TabIndex = 1;
            this.label24.Text = "INSIGHT";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(106, 54);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 1;
            this.label25.Text = "MEDICINE";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(115, 87);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 1;
            this.label26.Text = "PERCEPTION";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(108, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 12);
            this.label27.TabIndex = 1;
            this.label27.Text = "SURVIVAL";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(106, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 12);
            this.label29.TabIndex = 1;
            this.label29.Text = "DECEPTION";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(106, 36);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 12);
            this.label30.TabIndex = 1;
            this.label30.Text = "INTIMIDATION";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(106, 52);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 12);
            this.label31.TabIndex = 1;
            this.label31.Text = "PERFORMANCE";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(106, 68);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 12);
            this.label32.TabIndex = 1;
            this.label32.Text = "PERSUASION";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(106, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 12);
            this.label33.TabIndex = 1;
            this.label33.Text = "SAVING THROWS";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Enabled = false;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(87, 5);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(17, 11);
            this.textBox9.TabIndex = 3;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Enabled = false;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(87, 23);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(17, 10);
            this.textBox10.TabIndex = 3;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Enabled = false;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(87, 4);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(17, 10);
            this.textBox11.TabIndex = 3;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Enabled = false;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(87, 20);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(17, 10);
            this.textBox12.TabIndex = 3;
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Enabled = false;
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(87, 37);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(17, 10);
            this.textBox13.TabIndex = 3;
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox14.Enabled = false;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(87, 54);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(17, 10);
            this.textBox14.TabIndex = 3;
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.Enabled = false;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.Location = new System.Drawing.Point(87, 3);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(17, 10);
            this.textBox19.TabIndex = 3;
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.Enabled = false;
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.Location = new System.Drawing.Point(87, 5);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(17, 10);
            this.textBox20.TabIndex = 3;
            this.textBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox21.Enabled = false;
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.Location = new System.Drawing.Point(87, 22);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(17, 10);
            this.textBox21.TabIndex = 3;
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox22.Enabled = false;
            this.textBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.Location = new System.Drawing.Point(87, 39);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(17, 10);
            this.textBox22.TabIndex = 3;
            this.textBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.Enabled = false;
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.Location = new System.Drawing.Point(87, 56);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(17, 10);
            this.textBox23.TabIndex = 3;
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Enabled = false;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.Location = new System.Drawing.Point(87, 73);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(17, 10);
            this.textBox24.TabIndex = 3;
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.radioButton12);
            this.panel2.Controls.Add(this.radioButton13);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.textBox36);
            this.panel2.Controls.Add(this.textBox3);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.textBox9);
            this.panel2.Controls.Add(this.textBox10);
            this.panel2.Location = new System.Drawing.Point(25, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(203, 88);
            this.panel2.TabIndex = 5;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.FlatAppearance.BorderSize = 0;
            this.radioButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton12.Location = new System.Drawing.Point(72, 4);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(13, 12);
            this.radioButton12.TabIndex = 9;
            this.radioButton12.TabStop = true;
            this.radioButton12.UseVisualStyleBackColor = true;
            this.radioButton12.CheckedChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.FlatAppearance.BorderSize = 0;
            this.radioButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton13.Location = new System.Drawing.Point(72, 21);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(13, 12);
            this.radioButton13.TabIndex = 10;
            this.radioButton13.TabStop = true;
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(87, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 13);
            this.label20.TabIndex = 5;
            // 
            // textBox36
            // 
            this.textBox36.Enabled = false;
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(24, 3);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(28, 22);
            this.textBox36.TabIndex = 4;
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.radioButton8);
            this.panel3.Controls.Add(this.radioButton9);
            this.panel3.Controls.Add(this.radioButton10);
            this.panel3.Controls.Add(this.radioButton11);
            this.panel3.Controls.Add(this.textBox35);
            this.panel3.Controls.Add(this.textBox4);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.textBox14);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.textBox11);
            this.panel3.Controls.Add(this.textBox12);
            this.panel3.Controls.Add(this.textBox13);
            this.panel3.Location = new System.Drawing.Point(25, 185);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(203, 88);
            this.panel3.TabIndex = 5;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.FlatAppearance.BorderSize = 0;
            this.radioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton8.Location = new System.Drawing.Point(72, 52);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(13, 12);
            this.radioButton8.TabIndex = 6;
            this.radioButton8.TabStop = true;
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.FlatAppearance.BorderSize = 0;
            this.radioButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton9.Location = new System.Drawing.Point(72, 2);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(13, 12);
            this.radioButton9.TabIndex = 7;
            this.radioButton9.TabStop = true;
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.FlatAppearance.BorderSize = 0;
            this.radioButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton10.Location = new System.Drawing.Point(72, 18);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(13, 12);
            this.radioButton10.TabIndex = 8;
            this.radioButton10.TabStop = true;
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.FlatAppearance.BorderSize = 0;
            this.radioButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton11.Location = new System.Drawing.Point(72, 35);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(13, 12);
            this.radioButton11.TabIndex = 9;
            this.radioButton11.TabStop = true;
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // textBox35
            // 
            this.textBox35.Enabled = false;
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox35.Location = new System.Drawing.Point(24, 3);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(28, 22);
            this.textBox35.TabIndex = 4;
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.radioButton1);
            this.panel4.Controls.Add(this.textBox34);
            this.panel4.Controls.Add(this.textBox5);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.textBox19);
            this.panel4.Location = new System.Drawing.Point(25, 277);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(203, 88);
            this.panel4.TabIndex = 5;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.FlatAppearance.BorderSize = 0;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Location = new System.Drawing.Point(72, 1);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(13, 12);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // textBox34
            // 
            this.textBox34.Enabled = false;
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox34.Location = new System.Drawing.Point(24, 3);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(28, 22);
            this.textBox34.TabIndex = 4;
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.radioButton14);
            this.panel6.Controls.Add(this.radioButton15);
            this.panel6.Controls.Add(this.radioButton16);
            this.panel6.Controls.Add(this.radioButton17);
            this.panel6.Controls.Add(this.radioButton18);
            this.panel6.Controls.Add(this.textBox37);
            this.panel6.Controls.Add(this.textBox24);
            this.panel6.Controls.Add(this.textBox7);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.textBox23);
            this.panel6.Controls.Add(this.textBox20);
            this.panel6.Controls.Add(this.textBox22);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.textBox21);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Location = new System.Drawing.Point(25, 467);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(203, 88);
            this.panel6.TabIndex = 5;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.FlatAppearance.BorderSize = 0;
            this.radioButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton14.Location = new System.Drawing.Point(72, 71);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(13, 12);
            this.radioButton14.TabIndex = 6;
            this.radioButton14.TabStop = true;
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.FlatAppearance.BorderSize = 0;
            this.radioButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton15.Location = new System.Drawing.Point(72, 54);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(13, 12);
            this.radioButton15.TabIndex = 7;
            this.radioButton15.TabStop = true;
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.FlatAppearance.BorderSize = 0;
            this.radioButton16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton16.Location = new System.Drawing.Point(72, 3);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(13, 12);
            this.radioButton16.TabIndex = 8;
            this.radioButton16.TabStop = true;
            this.radioButton16.UseVisualStyleBackColor = true;
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.FlatAppearance.BorderSize = 0;
            this.radioButton17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton17.Location = new System.Drawing.Point(72, 20);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(13, 12);
            this.radioButton17.TabIndex = 9;
            this.radioButton17.TabStop = true;
            this.radioButton17.UseVisualStyleBackColor = true;
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.FlatAppearance.BorderSize = 0;
            this.radioButton18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton18.Location = new System.Drawing.Point(72, 37);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(13, 12);
            this.radioButton18.TabIndex = 10;
            this.radioButton18.TabStop = true;
            this.radioButton18.UseVisualStyleBackColor = true;
            // 
            // textBox37
            // 
            this.textBox37.Enabled = false;
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox37.Location = new System.Drawing.Point(24, 3);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(28, 22);
            this.textBox37.TabIndex = 4;
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.radioButton19);
            this.panel7.Controls.Add(this.radioButton20);
            this.panel7.Controls.Add(this.radioButton21);
            this.panel7.Controls.Add(this.radioButton22);
            this.panel7.Controls.Add(this.radioButton23);
            this.panel7.Controls.Add(this.textBox38);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.textBox8);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.textBox18);
            this.panel7.Controls.Add(this.textBox17);
            this.panel7.Controls.Add(this.label33);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.textBox6);
            this.panel7.Controls.Add(this.textBox15);
            this.panel7.Controls.Add(this.textBox16);
            this.panel7.Location = new System.Drawing.Point(25, 559);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(203, 88);
            this.panel7.TabIndex = 5;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.FlatAppearance.BorderSize = 0;
            this.radioButton19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton19.Location = new System.Drawing.Point(72, 70);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(13, 12);
            this.radioButton19.TabIndex = 11;
            this.radioButton19.TabStop = true;
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // radioButton20
            // 
            this.radioButton20.AutoSize = true;
            this.radioButton20.FlatAppearance.BorderSize = 0;
            this.radioButton20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton20.Location = new System.Drawing.Point(72, 54);
            this.radioButton20.Name = "radioButton20";
            this.radioButton20.Size = new System.Drawing.Size(13, 12);
            this.radioButton20.TabIndex = 12;
            this.radioButton20.TabStop = true;
            this.radioButton20.UseVisualStyleBackColor = true;
            // 
            // radioButton21
            // 
            this.radioButton21.AutoSize = true;
            this.radioButton21.FlatAppearance.BorderSize = 0;
            this.radioButton21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton21.Location = new System.Drawing.Point(72, 4);
            this.radioButton21.Name = "radioButton21";
            this.radioButton21.Size = new System.Drawing.Size(13, 12);
            this.radioButton21.TabIndex = 13;
            this.radioButton21.TabStop = true;
            this.radioButton21.UseVisualStyleBackColor = true;
            // 
            // radioButton22
            // 
            this.radioButton22.AutoSize = true;
            this.radioButton22.FlatAppearance.BorderSize = 0;
            this.radioButton22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton22.Location = new System.Drawing.Point(72, 20);
            this.radioButton22.Name = "radioButton22";
            this.radioButton22.Size = new System.Drawing.Size(13, 12);
            this.radioButton22.TabIndex = 14;
            this.radioButton22.TabStop = true;
            this.radioButton22.UseVisualStyleBackColor = true;
            // 
            // radioButton23
            // 
            this.radioButton23.AutoSize = true;
            this.radioButton23.FlatAppearance.BorderSize = 0;
            this.radioButton23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton23.Location = new System.Drawing.Point(72, 37);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(13, 12);
            this.radioButton23.TabIndex = 15;
            this.radioButton23.TabStop = true;
            this.radioButton23.UseVisualStyleBackColor = true;
            // 
            // textBox38
            // 
            this.textBox38.Enabled = false;
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox38.Location = new System.Drawing.Point(24, 3);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(28, 22);
            this.textBox38.TabIndex = 4;
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.Enabled = false;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(87, 72);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(17, 10);
            this.textBox18.TabIndex = 3;
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Enabled = false;
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.Location = new System.Drawing.Point(87, 55);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(17, 10);
            this.textBox17.TabIndex = 3;
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(87, 21);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(17, 10);
            this.textBox6.TabIndex = 3;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Enabled = false;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(87, 38);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(17, 10);
            this.textBox15.TabIndex = 3;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox16.Enabled = false;
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(87, 4);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(17, 10);
            this.textBox16.TabIndex = 3;
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.textBox39);
            this.panel8.Location = new System.Drawing.Point(25, 653);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(203, 42);
            this.panel8.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(73, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 30);
            this.label19.TabIndex = 4;
            this.label19.Text = "PASSIVE WISDOM \r\n   (PERCEPTION)";
            // 
            // textBox39
            // 
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox39.Location = new System.Drawing.Point(19, 6);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(40, 29);
            this.textBox39.TabIndex = 3;
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.textBox2);
            this.panel9.Controls.Add(this.textBox1);
            this.panel9.Location = new System.Drawing.Point(25, 10);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(203, 77);
            this.panel9.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label60);
            this.panel10.Controls.Add(this.radioButton5);
            this.panel10.Controls.Add(this.radioButton4);
            this.panel10.Controls.Add(this.radioButton3);
            this.panel10.Controls.Add(this.radioButton7);
            this.panel10.Controls.Add(this.radioButton6);
            this.panel10.Controls.Add(this.radioButton2);
            this.panel10.Controls.Add(this.label16);
            this.panel10.Controls.Add(this.label17);
            this.panel10.Controls.Add(this.label28);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this.textBox27);
            this.panel10.Controls.Add(this.label18);
            this.panel10.Controls.Add(this.label35);
            this.panel10.Controls.Add(this.textBox28);
            this.panel10.Controls.Add(this.textBox26);
            this.panel10.Controls.Add(this.textBox29);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Controls.Add(this.textBox30);
            this.panel10.Controls.Add(this.textBox31);
            this.panel10.Controls.Add(this.textBox32);
            this.panel10.Controls.Add(this.textBox33);
            this.panel10.Location = new System.Drawing.Point(25, 370);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(203, 93);
            this.panel10.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(16, 74);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(45, 15);
            this.label60.TabIndex = 6;
            this.label60.Text = "GENCE";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.FlatAppearance.BorderSize = 0;
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Location = new System.Drawing.Point(72, 76);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(13, 12);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.TabStop = true;
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.FlatAppearance.BorderSize = 0;
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Location = new System.Drawing.Point(72, 61);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(13, 12);
            this.radioButton4.TabIndex = 5;
            this.radioButton4.TabStop = true;
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.FlatAppearance.BorderSize = 0;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Location = new System.Drawing.Point(72, 46);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(13, 12);
            this.radioButton3.TabIndex = 5;
            this.radioButton3.TabStop = true;
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.FlatAppearance.BorderSize = 0;
            this.radioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton7.Location = new System.Drawing.Point(72, 1);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(13, 12);
            this.radioButton7.TabIndex = 5;
            this.radioButton7.TabStop = true;
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.FlatAppearance.BorderSize = 0;
            this.radioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton6.Location = new System.Drawing.Point(72, 16);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(13, 12);
            this.radioButton6.TabIndex = 5;
            this.radioButton6.TabStop = true;
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.FlatAppearance.BorderSize = 0;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Location = new System.Drawing.Point(72, 31);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(13, 12);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(106, 46);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 12);
            this.label28.TabIndex = 1;
            this.label28.Text = "INVESTIGATION";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(106, 61);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(44, 12);
            this.label34.TabIndex = 1;
            this.label34.Text = "NATURE";
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox27.Enabled = false;
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox27.Location = new System.Drawing.Point(87, 63);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(17, 10);
            this.textBox27.TabIndex = 3;
            this.textBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(106, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 12);
            this.label35.TabIndex = 1;
            this.label35.Text = "RELIGION";
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox28.Enabled = false;
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox28.Location = new System.Drawing.Point(87, 78);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(17, 10);
            this.textBox28.TabIndex = 3;
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox26
            // 
            this.textBox26.Enabled = false;
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox26.Location = new System.Drawing.Point(24, 3);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(28, 22);
            this.textBox26.TabIndex = 2;
            this.textBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox29
            // 
            this.textBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox29.Location = new System.Drawing.Point(19, 31);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(40, 29);
            this.textBox29.TabIndex = 2;
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox29.TextChanged += new System.EventHandler(this.textBox29_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(18, 61);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(46, 15);
            this.label36.TabIndex = 1;
            this.label36.Text = "INTELLI-";
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox30.Enabled = false;
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox30.Location = new System.Drawing.Point(87, 48);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(17, 10);
            this.textBox30.TabIndex = 3;
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox31.Enabled = false;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox31.Location = new System.Drawing.Point(87, 3);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(17, 10);
            this.textBox31.TabIndex = 3;
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox32.Enabled = false;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox32.Location = new System.Drawing.Point(87, 18);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(17, 10);
            this.textBox32.TabIndex = 3;
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox33.Enabled = false;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox33.Location = new System.Drawing.Point(87, 33);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(17, 10);
            this.textBox33.TabIndex = 3;
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 462);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(214, 22);
            this.label6.TabIndex = 1;
            this.label6.Text = "FEATURES";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Visible = false;
            // 
            // featurePanel
            // 
            this.featurePanel.BackColor = System.Drawing.Color.White;
            this.featurePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.featurePanel.Controls.Add(this.label6);
            this.featurePanel.Controls.Add(this.textBox25);
            this.featurePanel.Location = new System.Drawing.Point(476, 209);
            this.featurePanel.Name = "featurePanel";
            this.featurePanel.Size = new System.Drawing.Size(216, 486);
            this.featurePanel.TabIndex = 7;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox25.Location = new System.Drawing.Point(0, 0);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(214, 462);
            this.textBox25.TabIndex = 6;
            this.textBox25.TextChanged += new System.EventHandler(this.textBox25_TextChanged);
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox40.CausesValidation = false;
            this.textBox40.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox40.Location = new System.Drawing.Point(0, 0);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(183, 22);
            this.textBox40.TabIndex = 3;
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox40.TextChanged += new System.EventHandler(this.textBox40_TextChanged);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.textBox40);
            this.panel1.Location = new System.Drawing.Point(264, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(185, 39);
            this.panel1.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(0, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 15);
            this.label21.TabIndex = 4;
            this.label21.Text = "CHARACTER NAME";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.textBox53);
            this.panel5.Controls.Add(this.label41);
            this.panel5.Controls.Add(this.label40);
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.label37);
            this.panel5.Controls.Add(this.textBox45);
            this.panel5.Controls.Add(this.textBox44);
            this.panel5.Controls.Add(this.textBox43);
            this.panel5.Controls.Add(this.textBox42);
            this.panel5.Controls.Add(this.textBox41);
            this.panel5.Location = new System.Drawing.Point(517, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(396, 95);
            this.panel5.TabIndex = 9;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(166, 32);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(37, 15);
            this.label53.TabIndex = 25;
            this.label53.Text = "LEVEL";
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox53.Location = new System.Drawing.Point(169, 10);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(74, 19);
            this.textBox53.TabIndex = 24;
            this.textBox53.TextChanged += new System.EventHandler(this.textBox53_TextChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(246, 71);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(112, 15);
            this.label41.TabIndex = 23;
            this.label41.Text = "EXPERIENCE POINTS";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(128, 71);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(70, 15);
            this.label40.TabIndex = 22;
            this.label40.Text = "ALIGNMENT";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(9, 71);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(36, 15);
            this.label39.TabIndex = 21;
            this.label39.Text = "RACE";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(246, 32);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 15);
            this.label38.TabIndex = 20;
            this.label38.Text = "PLAYER NAME";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 32);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(40, 15);
            this.label37.TabIndex = 19;
            this.label37.Text = "CLASS";
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox45.Location = new System.Drawing.Point(249, 10);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(121, 19);
            this.textBox45.TabIndex = 18;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox44.Location = new System.Drawing.Point(249, 50);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(121, 19);
            this.textBox44.TabIndex = 17;
            this.textBox44.TextChanged += new System.EventHandler(this.textBox44_TextChanged);
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox43.Location = new System.Drawing.Point(131, 50);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(112, 19);
            this.textBox43.TabIndex = 16;
            this.textBox43.TextChanged += new System.EventHandler(this.textBox43_TextChanged);
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox42.Location = new System.Drawing.Point(12, 50);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(113, 19);
            this.textBox42.TabIndex = 15;
            this.textBox42.TextChanged += new System.EventHandler(this.textBox42_TextChanged);
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox41.Location = new System.Drawing.Point(12, 10);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(151, 19);
            this.textBox41.TabIndex = 14;
            this.textBox41.TextChanged += new System.EventHandler(this.textBox41_TextChanged);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.textBox52);
            this.panel11.Controls.Add(this.label52);
            this.panel11.Controls.Add(this.textBox51);
            this.panel11.Controls.Add(this.label51);
            this.panel11.Controls.Add(this.textBox50);
            this.panel11.Controls.Add(this.label50);
            this.panel11.Controls.Add(this.label49);
            this.panel11.Controls.Add(this.label48);
            this.panel11.Controls.Add(this.label47);
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.label45);
            this.panel11.Controls.Add(this.label44);
            this.panel11.Controls.Add(this.label43);
            this.panel11.Controls.Add(this.label42);
            this.panel11.Controls.Add(this.textBox49);
            this.panel11.Controls.Add(this.textBox48);
            this.panel11.Controls.Add(this.textBox47);
            this.panel11.Controls.Add(this.textBox46);
            this.panel11.Location = new System.Drawing.Point(249, 126);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(433, 74);
            this.panel11.TabIndex = 10;
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.Color.White;
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox52.Location = new System.Drawing.Point(335, 12);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(84, 29);
            this.textBox52.TabIndex = 35;
            this.textBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox52.TextChanged += new System.EventHandler(this.textBox52_TextChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(361, 48);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(33, 13);
            this.label52.TabIndex = 34;
            this.label52.Text = "SPEED";
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.Color.White;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox51.Location = new System.Drawing.Point(284, 12);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(36, 29);
            this.textBox51.TabIndex = 33;
            this.textBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox51.TextChanged += new System.EventHandler(this.textBox51_TextChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(277, 48);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(52, 13);
            this.label51.TabIndex = 32;
            this.label51.Text = "INITIATIVE";
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.White;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox50.Enabled = false;
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox50.Location = new System.Drawing.Point(14, 12);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(36, 29);
            this.textBox50.TabIndex = 31;
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(11, 41);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 26);
            this.label50.TabIndex = 30;
            this.label50.Text = "ARMOR\r\n CLASS";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(62, 23);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(12, 13);
            this.label49.TabIndex = 29;
            this.label49.Text = "=";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(114, 23);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(12, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "+";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(162, 23);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(12, 13);
            this.label47.TabIndex = 27;
            this.label47.Text = "+";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(210, 23);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(12, 13);
            this.label46.TabIndex = 26;
            this.label46.Text = "+";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(228, 48);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(27, 13);
            this.label45.TabIndex = 25;
            this.label45.Text = "Misc";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(177, 48);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(33, 13);
            this.label44.TabIndex = 24;
            this.label44.Text = "Shield";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(127, 48);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(34, 13);
            this.label43.TabIndex = 23;
            this.label43.Text = "Armor";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(74, 41);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(46, 26);
            this.label42.TabIndex = 22;
            this.label42.Text = "Dexterity\r\nModifier";
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox49.Location = new System.Drawing.Point(174, 20);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(36, 19);
            this.textBox49.TabIndex = 15;
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox49.TextChanged += new System.EventHandler(this.textBox49_TextChanged);
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox48.Location = new System.Drawing.Point(222, 20);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(36, 19);
            this.textBox48.TabIndex = 15;
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox48.TextChanged += new System.EventHandler(this.textBox48_TextChanged);
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox47.Enabled = false;
            this.textBox47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox47.Location = new System.Drawing.Point(78, 20);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(36, 19);
            this.textBox47.TabIndex = 15;
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox46.Location = new System.Drawing.Point(126, 20);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(36, 19);
            this.textBox46.TabIndex = 15;
            this.textBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox46.TextChanged += new System.EventHandler(this.textBox46_TextChanged);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.panel15);
            this.panel12.Controls.Add(this.panel17);
            this.panel12.Controls.Add(this.panel18);
            this.panel12.Location = new System.Drawing.Point(249, 206);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(214, 138);
            this.panel12.TabIndex = 11;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label79);
            this.panel15.Controls.Add(this.textBox77);
            this.panel15.Controls.Add(this.textBox75);
            this.panel15.Controls.Add(this.label80);
            this.panel15.Location = new System.Drawing.Point(-1, -3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(214, 74);
            this.panel15.TabIndex = 39;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(6, 13);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(75, 12);
            this.label79.TabIndex = 23;
            this.label79.Text = "Hit Point Maximum";
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.Color.White;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox77.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox77.Location = new System.Drawing.Point(14, 28);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(183, 29);
            this.textBox77.TabIndex = 37;
            this.textBox77.TextChanged += new System.EventHandler(this.textBox77_TextChanged);
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox75.Location = new System.Drawing.Point(86, 12);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(111, 15);
            this.textBox75.TabIndex = 22;
            this.textBox75.TextChanged += new System.EventHandler(this.textBox75_TextChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(68, 54);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(79, 12);
            this.label80.TabIndex = 24;
            this.label80.Text = "CURRENT HIT POINTS";
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.textBox78);
            this.panel17.Controls.Add(this.label82);
            this.panel17.Controls.Add(this.textBox76);
            this.panel17.Controls.Add(this.label81);
            this.panel17.Location = new System.Drawing.Point(-1, 70);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(106, 67);
            this.panel17.TabIndex = 40;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.Color.White;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox78.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox78.Location = new System.Drawing.Point(29, 24);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(52, 29);
            this.textBox78.TabIndex = 38;
            this.textBox78.TextChanged += new System.EventHandler(this.textBox78_TextChanged);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(16, 9);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(22, 12);
            this.label82.TabIndex = 26;
            this.label82.Text = "Total";
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox76.Location = new System.Drawing.Point(41, 6);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(57, 15);
            this.textBox76.TabIndex = 25;
            this.textBox76.TextChanged += new System.EventHandler(this.textBox76_TextChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(38, 53);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(35, 12);
            this.label81.TabIndex = 27;
            this.label81.Text = "HIT DICE";
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label85);
            this.panel18.Controls.Add(this.label83);
            this.panel18.Controls.Add(this.label84);
            this.panel18.Controls.Add(this.radioButton24);
            this.panel18.Controls.Add(this.radioButton27);
            this.panel18.Controls.Add(this.radioButton25);
            this.panel18.Controls.Add(this.radioButton28);
            this.panel18.Controls.Add(this.radioButton26);
            this.panel18.Controls.Add(this.radioButton29);
            this.panel18.Location = new System.Drawing.Point(104, 70);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(109, 67);
            this.panel18.TabIndex = 41;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(21, 52);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(55, 12);
            this.label85.TabIndex = 36;
            this.label85.Text = "DEATH SAVES";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(7, 13);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(40, 12);
            this.label83.TabIndex = 34;
            this.label83.Text = "SUCESSES";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(8, 31);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(39, 12);
            this.label84.TabIndex = 35;
            this.label84.Text = "FAILURES";
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.FlatAppearance.BorderSize = 0;
            this.radioButton24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton24.Location = new System.Drawing.Point(47, 13);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(13, 12);
            this.radioButton24.TabIndex = 28;
            this.radioButton24.TabStop = true;
            this.radioButton24.UseVisualStyleBackColor = true;
            // 
            // radioButton27
            // 
            this.radioButton27.AutoSize = true;
            this.radioButton27.FlatAppearance.BorderSize = 0;
            this.radioButton27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton27.Location = new System.Drawing.Point(77, 31);
            this.radioButton27.Name = "radioButton27";
            this.radioButton27.Size = new System.Drawing.Size(13, 12);
            this.radioButton27.TabIndex = 33;
            this.radioButton27.TabStop = true;
            this.radioButton27.UseVisualStyleBackColor = true;
            // 
            // radioButton25
            // 
            this.radioButton25.AutoSize = true;
            this.radioButton25.FlatAppearance.BorderSize = 0;
            this.radioButton25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton25.Location = new System.Drawing.Point(62, 13);
            this.radioButton25.Name = "radioButton25";
            this.radioButton25.Size = new System.Drawing.Size(13, 12);
            this.radioButton25.TabIndex = 29;
            this.radioButton25.TabStop = true;
            this.radioButton25.UseVisualStyleBackColor = true;
            // 
            // radioButton28
            // 
            this.radioButton28.AutoSize = true;
            this.radioButton28.FlatAppearance.BorderSize = 0;
            this.radioButton28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton28.Location = new System.Drawing.Point(62, 31);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(13, 12);
            this.radioButton28.TabIndex = 32;
            this.radioButton28.TabStop = true;
            this.radioButton28.UseVisualStyleBackColor = true;
            // 
            // radioButton26
            // 
            this.radioButton26.AutoSize = true;
            this.radioButton26.FlatAppearance.BorderSize = 0;
            this.radioButton26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton26.Location = new System.Drawing.Point(77, 13);
            this.radioButton26.Name = "radioButton26";
            this.radioButton26.Size = new System.Drawing.Size(13, 12);
            this.radioButton26.TabIndex = 30;
            this.radioButton26.TabStop = true;
            this.radioButton26.UseVisualStyleBackColor = true;
            // 
            // radioButton29
            // 
            this.radioButton29.AutoSize = true;
            this.radioButton29.FlatAppearance.BorderSize = 0;
            this.radioButton29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton29.Location = new System.Drawing.Point(47, 31);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(13, 12);
            this.radioButton29.TabIndex = 31;
            this.radioButton29.TabStop = true;
            this.radioButton29.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.textBox59);
            this.panel13.Controls.Add(this.label59);
            this.panel13.Controls.Add(this.textBox58);
            this.panel13.Controls.Add(this.label58);
            this.panel13.Controls.Add(this.textBox57);
            this.panel13.Controls.Add(this.label57);
            this.panel13.Location = new System.Drawing.Point(249, 350);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(214, 74);
            this.panel13.TabIndex = 12;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.Color.White;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox59.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox59.Location = new System.Drawing.Point(154, 10);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(36, 29);
            this.textBox59.TabIndex = 39;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(141, 45);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(68, 26);
            this.label59.TabIndex = 38;
            this.label59.Text = "SPELL ATTACK\r\n     BONUS";
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.Color.White;
            this.textBox58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox58.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox58.Location = new System.Drawing.Point(87, 10);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(36, 29);
            this.textBox58.TabIndex = 37;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(78, 45);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(56, 26);
            this.label58.TabIndex = 36;
            this.label58.Text = "SPELL SAVE\r\n      DC";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.Color.White;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox57.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox57.Location = new System.Drawing.Point(18, 10);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(36, 29);
            this.textBox57.TabIndex = 35;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(2, 45);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(71, 26);
            this.label57.TabIndex = 34;
            this.label57.Text = "SPELLCASTING\r\n      ABILITY";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label77);
            this.panel14.Controls.Add(this.label76);
            this.panel14.Controls.Add(this.label71);
            this.panel14.Controls.Add(this.textBox70);
            this.panel14.Controls.Add(this.label72);
            this.panel14.Controls.Add(this.textBox71);
            this.panel14.Controls.Add(this.label73);
            this.panel14.Controls.Add(this.textBox72);
            this.panel14.Controls.Add(this.label74);
            this.panel14.Controls.Add(this.textBox73);
            this.panel14.Controls.Add(this.label75);
            this.panel14.Controls.Add(this.textBox74);
            this.panel14.Controls.Add(this.label66);
            this.panel14.Controls.Add(this.textBox65);
            this.panel14.Controls.Add(this.label67);
            this.panel14.Controls.Add(this.textBox66);
            this.panel14.Controls.Add(this.label68);
            this.panel14.Controls.Add(this.textBox67);
            this.panel14.Controls.Add(this.label69);
            this.panel14.Controls.Add(this.textBox68);
            this.panel14.Controls.Add(this.label70);
            this.panel14.Controls.Add(this.textBox69);
            this.panel14.Controls.Add(this.label65);
            this.panel14.Controls.Add(this.textBox64);
            this.panel14.Controls.Add(this.label64);
            this.panel14.Controls.Add(this.textBox63);
            this.panel14.Controls.Add(this.label63);
            this.panel14.Controls.Add(this.textBox62);
            this.panel14.Controls.Add(this.label62);
            this.panel14.Controls.Add(this.textBox61);
            this.panel14.Controls.Add(this.label61);
            this.panel14.Controls.Add(this.textBox60);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(212, 241);
            this.panel14.TabIndex = 13;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(3, 150);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(205, 12);
            this.label77.TabIndex = 51;
            this.label77.Text = "========================================";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(3, 65);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(205, 12);
            this.label76.TabIndex = 50;
            this.label76.Text = "========================================";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(3, 198);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(33, 12);
            this.label71.TabIndex = 49;
            this.label71.Text = "RANGE";
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox70.Location = new System.Drawing.Point(5, 211);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(60, 15);
            this.textBox70.TabIndex = 48;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(67, 198);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(41, 12);
            this.label72.TabIndex = 47;
            this.label72.Text = "ATK BNUS";
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox71.Location = new System.Drawing.Point(69, 211);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(35, 15);
            this.textBox71.TabIndex = 46;
            this.textBox71.TextChanged += new System.EventHandler(this.textBox71_TextChanged);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(107, 198);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(41, 12);
            this.label73.TabIndex = 45;
            this.label73.Text = "DAMAGE";
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox72.Location = new System.Drawing.Point(109, 210);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(94, 15);
            this.textBox72.TabIndex = 44;
            this.textBox72.TextChanged += new System.EventHandler(this.textBox72_TextChanged);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(142, 167);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(21, 12);
            this.label74.TabIndex = 43;
            this.label74.Text = "TYPE";
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox73.Location = new System.Drawing.Point(144, 180);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(59, 15);
            this.textBox73.TabIndex = 42;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(3, 167);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(39, 12);
            this.label75.TabIndex = 41;
            this.label75.Text = "WEAPON";
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox74.Location = new System.Drawing.Point(5, 180);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(133, 15);
            this.textBox74.TabIndex = 40;
            this.textBox74.TextChanged += new System.EventHandler(this.textBox74_TextChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(3, 113);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(33, 12);
            this.label66.TabIndex = 39;
            this.label66.Text = "RANGE";
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox65.Location = new System.Drawing.Point(5, 126);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(60, 15);
            this.textBox65.TabIndex = 38;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(67, 113);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 12);
            this.label67.TabIndex = 37;
            this.label67.Text = "ATK BNUS";
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox66.Location = new System.Drawing.Point(69, 126);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(35, 15);
            this.textBox66.TabIndex = 36;
            this.textBox66.TextChanged += new System.EventHandler(this.textBox66_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(107, 113);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 12);
            this.label68.TabIndex = 35;
            this.label68.Text = "DAMAGE";
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox67.Location = new System.Drawing.Point(109, 125);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(94, 15);
            this.textBox67.TabIndex = 34;
            this.textBox67.TextChanged += new System.EventHandler(this.textBox67_TextChanged);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(142, 82);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(21, 12);
            this.label69.TabIndex = 33;
            this.label69.Text = "TYPE";
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox68.Location = new System.Drawing.Point(144, 95);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(59, 15);
            this.textBox68.TabIndex = 32;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(3, 82);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(39, 12);
            this.label70.TabIndex = 31;
            this.label70.Text = "WEAPON";
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox69.Location = new System.Drawing.Point(5, 95);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(133, 15);
            this.textBox69.TabIndex = 30;
            this.textBox69.TextChanged += new System.EventHandler(this.textBox69_TextChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(3, 27);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(33, 12);
            this.label65.TabIndex = 29;
            this.label65.Text = "RANGE";
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox64.Location = new System.Drawing.Point(5, 40);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(60, 15);
            this.textBox64.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(67, 27);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 12);
            this.label64.TabIndex = 27;
            this.label64.Text = "ATK BNUS";
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox63.Location = new System.Drawing.Point(69, 40);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(35, 15);
            this.textBox63.TabIndex = 26;
            this.textBox63.TextChanged += new System.EventHandler(this.textBox63_TextChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(107, 27);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 12);
            this.label63.TabIndex = 25;
            this.label63.Text = "DAMAGE";
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox62.Location = new System.Drawing.Point(109, 39);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(94, 15);
            this.textBox62.TabIndex = 24;
            this.textBox62.TextChanged += new System.EventHandler(this.textBox62_TextChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(142, -4);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(21, 12);
            this.label62.TabIndex = 23;
            this.label62.Text = "TYPE";
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox61.Location = new System.Drawing.Point(144, 9);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(59, 15);
            this.textBox61.TabIndex = 22;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(3, -4);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(39, 12);
            this.label61.TabIndex = 21;
            this.label61.Text = "WEAPON";
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox60.Location = new System.Drawing.Point(5, 9);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(133, 15);
            this.textBox60.TabIndex = 20;
            this.textBox60.TextChanged += new System.EventHandler(this.textBox60_TextChanged);
            // 
            // currPanel
            // 
            this.currPanel.BackColor = System.Drawing.Color.White;
            this.currPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.currPanel.Controls.Add(this.textBox56);
            this.currPanel.Controls.Add(this.label54);
            this.currPanel.Location = new System.Drawing.Point(699, 126);
            this.currPanel.Name = "currPanel";
            this.currPanel.Size = new System.Drawing.Size(214, 177);
            this.currPanel.TabIndex = 12;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox56.Location = new System.Drawing.Point(0, 0);
            this.textBox56.Multiline = true;
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(212, 162);
            this.textBox56.TabIndex = 39;
            this.textBox56.TextChanged += new System.EventHandler(this.textBox56_TextChanged);
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label54.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(0, 162);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(212, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "CURRENCY";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // accItemsPanel
            // 
            this.accItemsPanel.BackColor = System.Drawing.Color.White;
            this.accItemsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.accItemsPanel.Controls.Add(this.textBox55);
            this.accItemsPanel.Controls.Add(this.label55);
            this.accItemsPanel.Location = new System.Drawing.Point(699, 309);
            this.accItemsPanel.Name = "accItemsPanel";
            this.accItemsPanel.Size = new System.Drawing.Size(214, 177);
            this.accItemsPanel.TabIndex = 13;
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox55.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox55.Location = new System.Drawing.Point(0, 0);
            this.textBox55.Multiline = true;
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(212, 154);
            this.textBox55.TabIndex = 38;
            this.textBox55.TextChanged += new System.EventHandler(this.textBox55_TextChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(66, 157);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(81, 13);
            this.label55.TabIndex = 37;
            this.label55.Text = "ACESSIBLE ITEMS";
            // 
            // featsPanel
            // 
            this.featsPanel.BackColor = System.Drawing.Color.White;
            this.featsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.featsPanel.Controls.Add(this.textBox54);
            this.featsPanel.Controls.Add(this.label56);
            this.featsPanel.Location = new System.Drawing.Point(698, 492);
            this.featsPanel.Name = "featsPanel";
            this.featsPanel.Size = new System.Drawing.Size(214, 203);
            this.featsPanel.TabIndex = 14;
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox54.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox54.Location = new System.Drawing.Point(0, 0);
            this.textBox54.Multiline = true;
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(212, 179);
            this.textBox54.TabIndex = 39;
            this.textBox54.TextChanged += new System.EventHandler(this.textBox54_TextChanged);
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label56.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(0, 179);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(212, 22);
            this.label56.TabIndex = 38;
            this.label56.Text = "FEATS";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.White;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label78);
            this.panel16.Controls.Add(this.panel14);
            this.panel16.Location = new System.Drawing.Point(249, 430);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(214, 265);
            this.panel16.TabIndex = 16;
            // 
            // label78
            // 
            this.label78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label78.Font = new System.Drawing.Font("Century Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(0, 241);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(212, 22);
            this.label78.TabIndex = 14;
            this.label78.Text = "ATTACKS";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label78.Visible = false;
            // 
            // CharacterSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.featsPanel);
            this.Controls.Add(this.accItemsPanel);
            this.Controls.Add(this.currPanel);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.featurePanel);
            this.DoubleBuffered = true;
            this.Name = "CharacterSheet";
            this.Size = new System.Drawing.Size(929, 713);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.featurePanel.ResumeLayout(false);
            this.featurePanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.currPanel.ResumeLayout(false);
            this.currPanel.PerformLayout();
            this.accItemsPanel.ResumeLayout(false);
            this.accItemsPanel.PerformLayout();
            this.featsPanel.ResumeLayout(false);
            this.featsPanel.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel featurePanel;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel currPanel;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel accItemsPanel;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel featsPanel;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton radioButton20;
        private System.Windows.Forms.RadioButton radioButton21;
        private System.Windows.Forms.RadioButton radioButton22;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.RadioButton radioButton27;
        private System.Windows.Forms.RadioButton radioButton25;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.RadioButton radioButton26;
        private System.Windows.Forms.RadioButton radioButton29;
    }
}
