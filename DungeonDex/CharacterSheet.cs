﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DungeonDex.Utils;

namespace DungeonDex
{
    public partial class CharacterSheet : UserControl
    {
        public CharacterModel c;

        public CharacterSheet()
        { 
            InitializeComponent();
        }

        public CharacterSheet(ref CharacterModel c)
        {        
            InitializeComponent();
        }

        private int CalculateMod(int i)
        {
            return Convert.ToInt16(Math.Floor(Convert.ToDecimal((i - 10) / 2)));
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void SetCharacter(ref CharacterModel outsideCharacter)
        {
            c = outsideCharacter;
            textBox1.Text = "0";
        }

        //Update AC
        private void UpdateAC()
        {
            int dexmod = (textBox47.Text != "") ? Convert.ToInt16(textBox47.Text) : 0;
            int armor = (textBox46.Text != "") ? Convert.ToInt16(textBox46.Text) : 0;
            int shield = (textBox49.Text != "") ? Convert.ToInt16(textBox49.Text) : 0;
            int misc = (textBox48.Text != "") ? Convert.ToInt16(textBox48.Text) : 0;
            textBox50.Text = (dexmod + armor + shield + misc).ToString();
        }

        public void Reload(CharacterModel newChar)
        {
            //load abilities
            textBox3.Text = newChar.Str.ToString();
            textBox4.Text = newChar.Dex.ToString();
            textBox5.Text = newChar.Constitution.ToString();
            textBox29.Text = newChar.Intel.ToString();
            textBox7.Text = newChar.Wisdom.ToString();
            textBox8.Text = newChar.Charisma.ToString();

            //load top bar
            textBox41.Text = newChar.C_class.ToString();
            textBox53.Text = newChar.Level.ToString();
            textBox42.Text = newChar.Race.ToString();
            textBox43.Text = newChar.Alignment.ToString();
            textBox44.Text = newChar.Exp.ToString();
            textBox40.Text = newChar.Name.ToString();

            //just below it
            textBox56.Text = newChar.Items.ToString();
            textBox55.Text = newChar.A_items.ToString();
            textBox54.Text = newChar.Feats.ToString();
            textBox25.Text = newChar.Features.ToString();

            //top middle
            textBox49.Text = newChar.Shield.ToString();
            textBox48.Text = newChar.Ac_misc.ToString();
            textBox46.Text = newChar.Ac.ToString();
            textBox52.Text = newChar.Spd.ToString();

            //just below that
            textBox75.Text = newChar.Hp_max.ToString();
            textBox77.Text = newChar.Hp_current.ToString();
            textBox76.Text = newChar.Hitdie_count.ToString();
            textBox78.Text = newChar.Hitdie_type.ToString();

            //Weapon 1
            textBox60.Text = newChar.W1_name.ToString();
            textBox63.Text = newChar.W1_atkbns.ToString();
            textBox62.Text = newChar.W1_damagetype.ToString();

            //Weapon 2
            textBox69.Text = newChar.W2_name.ToString();
            textBox66.Text = newChar.W2_atkbns.ToString();
            textBox67.Text = newChar.W2_damagetype.ToString();

            //Weapon 3
            textBox74.Text = newChar.W3_name.ToString();
            textBox71.Text = newChar.W3_atkbns.ToString();
            textBox72.Text = newChar.W3_damagetype.ToString();

            c = newChar;
            c.ID1 = newChar.ID1;
        }

        internal void Clear()
        {
            textBox1.Text = "0";
            //clear abilities
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox29.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";

            //clear top bar
            textBox41.Text = "";
            textBox53.Text = "";
            textBox42.Text = "";
            textBox43.Text = "";
            textBox44.Text = "";
            textBox40.Text = "";

            //just below it
            textBox56.Text = "";
            textBox55.Text = "";
            textBox54.Text = "";
            textBox25.Text = "";

            //top middle
            textBox49.Text = "";
            textBox48.Text = "";
            textBox46.Text = "";
            textBox52.Text = "";

            //just below that
            textBox75.Text = "";
            textBox77.Text = "";
            textBox76.Text = "";
            textBox78.Text = "";

            //Weapon 1
            textBox60.Text = "";
            textBox63.Text = "";
            textBox62.Text = "";

            //Weapon 2
            textBox69.Text = "";
            textBox66.Text = "";
            textBox67.Text = "";

            //Weapon 3
            textBox74.Text = "";
            textBox71.Text = "";
            textBox72.Text = "";

            c = new CharacterModel();
        }

        //Strength
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox36.Text = CalculateMod(Convert.ToInt16(textBox3.Text)).ToString();
                c.Str = Convert.ToInt16(textBox3.Text);

            }
            else
            {
                textBox36.Text = "";
                c.Str = 0;
            }

            //Set Ath
            if (!radioButton13.Checked)
            {
                textBox10.Text = textBox36.Text;
            }
            else if (textBox36.Text != "")
            {
                textBox10.Text = (Convert.ToInt16(textBox36.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }


            if (!radioButton12.Checked)
            {
                textBox9.Text = textBox36.Text;
            }
            else if (textBox36.Text != "")
            {
                textBox9.Text = (Convert.ToInt16(textBox36.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

        }

        //Dexterity
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

            if (textBox4.Text != "")
            {
                textBox35.Text = CalculateMod(Convert.ToInt16(textBox4.Text)).ToString();
                c.Dex = Convert.ToInt16(textBox4.Text);
            }
            else
            {
                textBox35.Text = "";
                c.Dex = Convert.ToInt16(0);
            }
            textBox51.Text = textBox35.Text;
            textBox47.Text = textBox35.Text;
            UpdateAC();

            if (!radioButton9.Checked)
            {
                textBox11.Text = textBox35.Text;
            }
            else if (textBox35.Text != "")
            {
                textBox11.Text = (Convert.ToInt16(textBox35.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
            //

            if (!radioButton10.Checked)
            {
                textBox12.Text = textBox35.Text;
            }
            else if(textBox35.Text != "")
            {
                textBox12.Text = (Convert.ToInt16(textBox35.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton11.Checked)
            {
                textBox13.Text = textBox35.Text;
            }
            else if (textBox35.Text != "")
            {
                textBox13.Text = (Convert.ToInt16(textBox35.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton8.Checked)
            {
                textBox14.Text = textBox35.Text;
            }
            else if (textBox35.Text != "")
            {
                textBox14.Text = (Convert.ToInt16(textBox35.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
        }

        //Constitution
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.Text != "")
            {
                textBox34.Text = CalculateMod(Convert.ToInt16(textBox5.Text)).ToString();
                c.Constitution = Convert.ToInt16(textBox5.Text);
            }
            else
            {
                textBox34.Text = "";
                c.Constitution = Convert.ToInt16(0);
            }

            if (!radioButton1.Checked)
            {
                textBox19.Text = textBox34.Text;
            }
            else if (textBox34.Text != "")
            {
                textBox19.Text = (Convert.ToInt16(textBox34.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
        }

        //Intelligence
        private void textBox29_TextChanged(object sender, EventArgs e)
        {
            if (textBox29.Text != "")
            {
                textBox26.Text = CalculateMod(Convert.ToInt16(textBox29.Text)).ToString();
                c.Intel = Convert.ToInt16(textBox29.Text);
            }
            else
            {
                textBox26.Text = "";
                c.Intel = Convert.ToInt16(0);
            }

            //////////////////////////////////////////////////////////////////////////

            if (!radioButton7.Checked)
            {
                textBox31.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox31.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton6.Checked)
            {
                textBox32.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox32.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }


            if (!radioButton2.Checked)
            {
                textBox33.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox33.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton3.Checked)
            {
                textBox30.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox30.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton4.Checked)
            {
                textBox27.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox27.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton5.Checked)
            {
                textBox28.Text = textBox26.Text;
            }
            else if (textBox26.Text != "")
            {
                textBox28.Text = (Convert.ToInt16(textBox26.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
        }

        //Wisdom
        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            if (textBox7.Text != "")
            {
                textBox37.Text = CalculateMod(Convert.ToInt16(textBox7.Text)).ToString();
                c.Wisdom = Convert.ToInt16(textBox7.Text);
            }
            else
            {
                textBox37.Text = "";
                c.Wisdom = Convert.ToInt16(0);
            }

            //////////////////////////////////////////////////////////////////////////

            if (!radioButton16.Checked)
            {
                textBox20.Text = textBox37.Text;
            }
            else if (textBox37.Text != "")
            {
                textBox20.Text = (Convert.ToInt16(textBox37.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton17.Checked)
            {
                textBox21.Text = textBox37.Text;
            }
            else if (textBox37.Text != "")
            {
                textBox21.Text = (Convert.ToInt16(textBox37.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton18.Checked)
            {
                textBox22.Text = textBox37.Text;
            }
            else if (textBox37.Text != "")
            {
                textBox22.Text = (Convert.ToInt16(textBox37.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton15.Checked)
            {
                textBox23.Text = textBox37.Text;
            }
            else if (textBox37.Text != "")
            {
                textBox23.Text = (Convert.ToInt16(textBox37.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton14.Checked)
            {
                textBox24.Text = textBox37.Text;
            }
            else if (textBox37.Text != "")
            {
                textBox24.Text = (Convert.ToInt16(textBox37.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
        }

        //Charisma
        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            if (textBox8.Text != "")
            {
                textBox38.Text = CalculateMod(Convert.ToInt16(textBox8.Text)).ToString();
                c.Charisma = Convert.ToInt16(textBox8.Text);
            }
            else
            {
                textBox38.Text = "";
                c.Charisma = Convert.ToInt16(0);
            }

            ///////////////////////////////////////////////////////////////////////////

            if (!radioButton21.Checked)
            {
                textBox16.Text = textBox38.Text;
            }
            else if (textBox38.Text != "")
            {
                textBox16.Text = (Convert.ToInt16(textBox38.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton22.Checked)
            {
                textBox6.Text = textBox38.Text;
            }
            else if (textBox38.Text != "")
            {
                textBox6.Text = (Convert.ToInt16(textBox38.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton23.Checked)
            {
                textBox15.Text = textBox38.Text;
            }
            else if (textBox38.Text != "")
            {
                textBox15.Text = (Convert.ToInt16(textBox38.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton20.Checked)
            {
                textBox17.Text = textBox38.Text;
            }
            else if (textBox38.Text != "")
            {
                textBox17.Text = (Convert.ToInt16(textBox38.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }

            if (!radioButton19.Checked)
            {
                textBox18.Text = textBox38.Text;
            }
            else if (textBox38.Text != "")
            {
                textBox18.Text = (Convert.ToInt16(textBox38.Text) + Convert.ToInt16(textBox1.Text)).ToString();
            }
        }

        //Name
        private void textBox40_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Name = textBox40.Text;
            }
            else
            {
                textBox40.Text = "";
                c.Name = "";
            }
        }

        //Armor
        private void textBox46_TextChanged(object sender, EventArgs e)
        {
            if (textBox46.Text != "")
            {
                c.Ac = Convert.ToInt16(textBox46.Text);
            }
            else
            {
                textBox46.Text = "";
                c.Ac = Convert.ToInt16(0);
            }
            UpdateAC();

        }
        //Shield
        private void textBox49_TextChanged(object sender, EventArgs e)
        {
            UpdateAC();
        }
        //MISC
        private void textBox48_TextChanged(object sender, EventArgs e)
        {
            UpdateAC();
        }

        //Class
        private void textBox41_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.C_class = textBox41.Text;
            }
            else
            {
                textBox41.Text = "";
                c.C_class = "";
            }
        }

        //Race
        private void textBox42_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Race = textBox42.Text;
            }
            else
            {
                textBox42.Text = "";
                c.Race = "";
            }
        }

        //Alignment
        private void textBox43_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Alignment = textBox43.Text;
            }
            else
            {
                textBox43.Text = "";
                c.Alignment = "";
            }
        }

        //AC
        private void textBox50_TextChanged(object sender, EventArgs e)
        {
           TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Ac = Convert.ToInt16(textBox50.Text);

            }
            else
            {
                textBox50.Text = "";
                c.Ac = 0;
            }
        }

        //Initiative
        private void textBox51_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Initiative = Convert.ToInt16(textBox51.Text);

            }
            else
            {
                textBox51.Text = "";
                c.Initiative = 0;
            }
        }

        //Speed
        private void textBox52_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Spd = Convert.ToInt16(textBox52.Text);

            }
            else
            {
                textBox52.Text = "";
                c.Spd = 0;
            }
        }

        //Hp MAX
        private void textBox75_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Hp_max = Convert.ToInt16(textBox75.Text);

            }
            else
            {
                textBox75.Text = "";
                c.Hp_max = 0;
            }
        }

        //HP_current
        private void textBox77_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Hp_current = Convert.ToInt16(textBox77.Text);

            }
            else
            {
                textBox77.Text = "";
                c.Hp_current = 0;
            }
        }

        //hit die total
        private void textBox76_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Hitdie_count = Convert.ToInt16(textBox76.Text);

            }
            else
            {
                textBox76.Text = "";
                c.Hitdie_count = 0;
            }
        }
        
        //hitdie type
        private void textBox78_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.Hitdie_type = Convert.ToInt16(textBox78.Text);

            }
            else
            {
                textBox78.Text = "";
                c.Hitdie_type = 0;
            }
        }

        //Weapon1 name
        private void textBox60_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W1_name = textBox60.Text;

            }
            else
            {
                textBox60.Text = "";
                c.W1_name = "";
            }
        }
        
        //Weapon 2 name
        private void textBox69_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W2_name = textBox69.Text;

            }
            else
            {
                textBox69.Text = "";
                c.W2_name = "";
            }
        }

        //Weapon 3 Name
        private void textBox74_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W3_name = textBox74.Text;

            }
            else
            {
                textBox74.Text = "";
                c.W3_name = "";
            }
        }

        //Weapon 1 Damage
        private void textBox62_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W1_damagetype = textBox62.Text;

            }
            else
            {
                textBox62.Text = "";
                c.W1_damagetype = "";
            }
        }

        //Weapon 2 Damage
        private void textBox67_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W2_damagetype = textBox67.Text;

            }
            else
            {
                textBox67.Text = "";
                c.W2_damagetype = "";
            }
        }

        //Weapon 3 Damage
        private void textBox72_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W3_damagetype = textBox72.Text;

            }
            else
            {
                textBox72.Text = "";
                c.W3_damagetype = "";
            }
        }

        //Weapon 1 Atk BNS
        private void textBox63_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W1_atkbns = Convert.ToInt16(textBox63.Text);

            }
            else
            {
                textBox63.Text = "";
                c.W1_atkbns = 0;
            }
        }

        //Weapon 2 Atk BNS
        private void textBox66_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W2_atkbns = Convert.ToInt16(textBox66.Text);

            }
            else
            {
                textBox66.Text = "";
                c.W2_atkbns = 0;
            }
        }

        //Weapon 3 Atk BNS
        private void textBox71_TextChanged(object sender, EventArgs e)
        {
            TextBox s = sender as TextBox;

            if (s.Text != "")
            {
                c.W3_atkbns = Convert.ToInt16(textBox71.Text);

            }
            else
            {
                textBox71.Text = "";
                c.W3_atkbns = 0;
            }
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                textBox1.Text = "0";
                c.ProfBonus = Convert.ToInt16(textBox1.Text);
            }
            else
            {
                c.ProfBonus = Convert.ToInt16(textBox1.Text);
            }
        }

        private void textBox44_TextChanged(object sender, EventArgs e)
        {
            if (textBox44.Text != "")
            {
                c.Exp = Convert.ToInt16(textBox44.Text);

            }
            else
            {
                textBox44.Text = "";
                c.Exp = 0;
               
            }
        }

        private void textBox53_TextChanged(object sender, EventArgs e)
        {
            if (textBox53.Text != "")
            {
                c.Level = Convert.ToInt16(textBox53.Text);

            }
            else
            {
                textBox53.Text = "";
                c.Level = 0;

            }
        }

        private void textBox25_TextChanged(object sender, EventArgs e)
        {
            if (textBox25.Text != "")
            {
                c.Features = textBox25.Text;
            }
        }

        private void textBox56_TextChanged(object sender, EventArgs e)
        {
            if (textBox56.Text != "")
            {
                c.Items = textBox56.Text;
            }
        }

        private void textBox55_TextChanged(object sender, EventArgs e)
        {
            if (textBox56.Text != "")
                c.A_items = textBox55.Text;
        }

        private void textBox54_TextChanged(object sender, EventArgs e)
        {
            if (textBox56.Text != "")
                c.Feats = textBox54.Text;
        }
    }
}
