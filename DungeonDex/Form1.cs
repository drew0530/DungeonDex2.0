﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDex
{
    public partial class Form1 : Form
    {
        static Utils.DatabaseHandler dbHandler = new Utils.DatabaseHandler();
        Utils.CharacterModel c;

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(ref Utils.CharacterModel ch)
        {
            InitializeComponent();
            c = ch;
        }

        ~Form1()
        {
            dbHandler.SaveCharacter(c);
        }


// Color Change Event Listeners for Toolbar==//
        private void buttonClose_MouseDown(object sender, MouseEventArgs e)
        {
            buttonClose.BackColor = Color.Red;
        }
        private void buttonMin_MouseDown(object sender, MouseEventArgs e)
        {
            buttonMin.BackColor = Color.Red;

        }
        private void buttonMax_MouseDown(object sender, MouseEventArgs e)
        {
            buttonMax.BackColor = Color.Red;
        }
        private void buttonClose_MouseUp(object sender, MouseEventArgs e)
        {
            buttonClose.BackColor = Color.Transparent;
        }
        private void buttonMin_MouseUp(object sender, MouseEventArgs e)
        {
            buttonMin.BackColor = Color.Transparent;
        }
        private void buttonMax_MouseUp(object sender, MouseEventArgs e)
        {
            buttonMax.BackColor = Color.Transparent;
        }                                         
//=========================================//

        private bool mouseDown;
        private Point lastLocation;
        
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void buttonMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void characterSheet1_Load(object sender, EventArgs e)
        {
            characterSheet1.SetCharacter(ref c);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dbHandler.SaveCharacter(characterSheet1.c);
            MessageBox.Show("Character has (should have been) saved!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var LoadPopUp = new LoadForm(ref dbHandler, ref characterSheet1, ref c);
            LoadPopUp.Show();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            characterSheet1.Clear();
        }
    }
}