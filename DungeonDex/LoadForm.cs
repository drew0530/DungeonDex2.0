﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDex
{
    public partial class LoadForm : Form
    {
        Utils.DatabaseHandler db;
        CharacterSheet cs;
        Utils.CharacterModel c;

        public LoadForm()
        {
            InitializeComponent();
        }

        //This constructor will fill the ListBox with data from the DB
        public LoadForm(ref Utils.DatabaseHandler d, ref CharacterSheet cso, ref Utils.CharacterModel cm)
        {
            InitializeComponent();
            db = d;
            cs = cso;
            c = cm;
            listBox1.DataSource = d.ListCharacters().Values.ToList();
        }

        // Start of Panel Move Script

        private bool mouseDown;
        private Point lastLocation;

        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void panel3_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        // End of Panel Move Script

        //Open button Click
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            var index = listBox1.SelectedIndex;
            var value = listBox1.SelectedItem;
            c = db.LoadCharacter(db.ListCharacters().Keys.ElementAt(index));
            cs.Reload(c);
            
        }


        //Cancel button click
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
